/**
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

/**
 * Application:
 * AI based Pose Detection on Live stream.
 *
 * Description:
 * The application takes live video stream from camera and gives same to
 * posenet mobilenet v1 TensorFlow Lite Model for pose detection and
 * display preview with overlayed AI Model outout/classification labels.
 *
 * Pipeline for Gstreamer:
 * qtiqmmfsrc (Camera) -> main_capsfilter -> tee (SPLIT)
 *     | tee -> qtivcomposer
 *     |     -> Pre process-> ML Framework -> Post process -> qtivcomposer
 *     qtivcomposer (COMPOSITION) -> waylandsink (Display)
 *     Pre process: qtimlvconverter
 *     ML Framework: qtimlsnpe/qtimltflite
 *     Post process: qtimlvpose -> detection_filter
 */

#include <stdio.h>
#include <glib-unix.h>
#include <gst/gst.h>

#include "include/gst_sample_apps_utils.h"

/**
 * Default models and labels path, if not provided by user
 */
#define DEFAULT_TFLITE_POSE_DETECTION_MODEL "/opt/posenet_mobilenet_v1.tflite"
#define DEFAULT_POSE_DETECTION_LABELS "/opt/posenet_mobilenet_v1.labels"

/**
 * Default setting of camera output resolution, Scaling of camera output
 * will be done in qtimlvconverter based on model input
 */
#define DEFAULT_CAMERA_OUTPUT_WIDTH 1280
#define DEFAULT_CAMERA_OUTPUT_HEIGHT 720
#define DEFAULT_CAMERA_FRAME_RATE 30

/**
 * Number of Queues used for buffer caching between elements
 */
#define QUEUE_COUNT 7

/**
 * Create GST pipeline: has 3 main steps
 * 1. Create all elements/GST Plugins
 * 2. Set Paramters for each plugin
 * 3. Link plugins to create GST pipeline
 *
 * @param appctx Application Context Pointer.
 * @param model_type Type of Model container for the Runtime.
 * @param model_path Location of Model Container.
 * @param labels_path Location of Model Labels.
 */
static gboolean
create_pipe (GstAppContext * appctx, ModelType model_type,
    const char * model_path, const char * labels_path)
{
  GstElement *qtiqmmfsrc, *main_capsfilter, *queue[QUEUE_COUNT];
  GstElement *tee, *qtimlvconverter, *qtimlelement;
  GstElement *qtimlvpose, *detection_filter;
  GstElement *qtivcomposer, *waylandsink;
  GstCaps *pad_filter, *filtercaps;
  GstStructure *delegate_options;
  gint width = DEFAULT_CAMERA_OUTPUT_WIDTH;
  gint height = DEFAULT_CAMERA_OUTPUT_HEIGHT;
  gint framerate = DEFAULT_CAMERA_FRAME_RATE;
  gint module_enum;
  gchar element_name[128];
  gboolean ret = FALSE;

  // 1. Create the elements or Plugins

  // get live camera stream using qtiqmmfsrc plugin
  qtiqmmfsrc = gst_element_factory_make ("qtiqmmfsrc", "qtiqmmfsrc");
  if (!qtiqmmfsrc) {
    g_printerr ("Failed to create qtiqmmfsrc\n");
    return FALSE;
  }

  // Use capsfilter to define the camera output settings
  main_capsfilter = gst_element_factory_make ("capsfilter", "main_capsfilter");
  if (!main_capsfilter) {
    g_printerr ("Failed to create main_capsfilter\n");
    return FALSE;
  }

  // Creating queue to decouple the processing on sink and source pad.
  for (int i = 0; i < QUEUE_COUNT; i++) {
    snprintf (element_name, 127, "queue-%d", i);
    queue[i] = gst_element_factory_make ("queue", element_name);
    if (!queue[i]) {
      g_printerr ("Failed to create queue %d\n", i);
      return FALSE;
    }
  }

  // Use tee to send same data buffer
  // one for AI inferencing, one for Display composition
  tee = gst_element_factory_make ("tee", "tee");
  if (!tee) {
    g_printerr ("Failed to create tee\n");
    return FALSE;
  }

  // Creating qtimlvconverter for Input preprocessing
  qtimlvconverter = gst_element_factory_make ("qtimlvconverter",
      "qtimlvconverter");
  if (!qtimlvconverter) {
    g_printerr ("Failed to create qtimlvconverter\n");
    return FALSE;
  }

  // Creating the ML inferencing plugin
  qtimlelement = gst_element_factory_make ("qtimltflite", "qtimltflite");
  if (!qtimlelement) {
    g_printerr ("Failed to create qtimlelement\n");
    return FALSE;
  }

  // Creating plugin for ML postprocessing for pose detection
  qtimlvpose = gst_element_factory_make ("qtimlvpose",
      "qtimlvpose");
  if (!qtimlvpose) {
    g_printerr ("Failed to create qtimlvpose\n");
    return FALSE;
  }

  // Composer to combine camera output with ML post proc output
  qtivcomposer = gst_element_factory_make ("qtivcomposer", "qtivcomposer");
  if (!qtivcomposer) {
    g_printerr ("Failed to create qtivcomposer\n");
    return FALSE;
  }

  // Used to negotiate between ML post proc o/p and qtivcomposer
  detection_filter = gst_element_factory_make ("capsfilter", "detection_filter");
  if (!detection_filter) {
    g_printerr ("Failed to create detection_filter\n");
    return FALSE;
  }

  // Creating Wayland compositor to render output on Display
  waylandsink = gst_element_factory_make ("waylandsink", "waylandsink");
  if (!waylandsink) {
    g_printerr ("Failed to create waylandsink \n");
    return FALSE;
  }

  // 1.1 Append all elements in a list for cleanup
  appctx->plugins = NULL;
  appctx->plugins = g_list_append (appctx->plugins, qtiqmmfsrc);
  appctx->plugins = g_list_append (appctx->plugins, main_capsfilter);
  appctx->plugins = g_list_append (appctx->plugins, tee);
  appctx->plugins = g_list_append (appctx->plugins, qtimlvconverter);
  appctx->plugins = g_list_append (appctx->plugins, qtimlelement);
  appctx->plugins = g_list_append (appctx->plugins, qtimlvpose);
  appctx->plugins = g_list_append (appctx->plugins, detection_filter);
  appctx->plugins = g_list_append (appctx->plugins, qtivcomposer);
  appctx->plugins = g_list_append (appctx->plugins, waylandsink);

  for (int i = 0; i < QUEUE_COUNT; i++) {
    appctx->plugins = g_list_append (appctx->plugins, queue[i]);
  }

  // 2. Set properties for all GST plugin elements

  // 2.1 Set the capabilities of camera plugin output
  filtercaps = gst_caps_new_simple ("video/x-raw",
      "format", G_TYPE_STRING, "NV12",
      "width", G_TYPE_INT, width, "height", G_TYPE_INT, height,
      "framerate", GST_TYPE_FRACTION, framerate, 1,
      "compression", G_TYPE_STRING, "ubwc", NULL);
  gst_caps_set_features (filtercaps, 0,
      gst_caps_features_new ("memory:GBM", NULL));
  g_object_set (G_OBJECT (main_capsfilter), "caps", filtercaps, NULL);
  gst_caps_unref (filtercaps);

  // 2.2 Selecting the HW to DSP for model inferencing using delegate property
  delegate_options = gst_structure_from_string (
      "QNNExternalDelegate,backend_type=htp;", NULL);
  g_object_set (G_OBJECT (qtimlelement), "model", model_path,
      "delegate", GST_ML_TFLITE_DELEGATE_EXTERNAL, NULL);
  g_object_set (G_OBJECT (qtimlelement),
      "external-delegate-path", "libQnnTFLiteDelegate.so", NULL);
  g_object_set (G_OBJECT (qtimlelement),
      "external-delegate-options", delegate_options, NULL);
  gst_structure_free (delegate_options);

  // 2.3 Set properties for ML postproc plugins- module, layers, threshold
  module_enum = get_enum_value (qtimlvpose, "module" , "posenet");
  if (module_enum != -1 ) {
    g_object_set (G_OBJECT (qtimlvpose), "threshold", 51.0, "results", 2,
        "module", module_enum, "labels", labels_path, "constants",
        "Posenet,q-offsets=<128.0,128.0,117.0>,"
        "q-scales=<0.0784313753247261,0.0784313753247261,1.3875764608383179>;",
        NULL);
  } else {
    g_printerr ("Module posenet in not available in qtimlvpose.\n");
    goto error;
  }

  // 2.4 Set the properties of Wayland compositor
  g_object_set (G_OBJECT (waylandsink), "sync", FALSE, NULL);
  g_object_set (G_OBJECT (waylandsink), "fullscreen", true, NULL);

  // Setting the properties of pad_filter for negotiation with qtivcomposer
  pad_filter = gst_caps_new_simple ("video/x-raw",
      "format", G_TYPE_STRING, "BGRA",
      "width", G_TYPE_INT, 640,
      "height", G_TYPE_INT, 360, NULL);

  g_object_set (G_OBJECT (detection_filter), "caps", pad_filter, NULL);
  gst_caps_unref (pad_filter);

  // 3. Setup the pipeline

  g_print ("Adding all elements to the pipeline...\n");

  gst_bin_add_many (GST_BIN (appctx->pipeline), qtiqmmfsrc, main_capsfilter,
      tee, qtimlvconverter, qtimlelement, qtimlvpose,
      detection_filter, qtivcomposer, waylandsink, NULL);

  for (int i = 0; i < QUEUE_COUNT; i++) {
    gst_bin_add_many (GST_BIN (appctx->pipeline), queue[i], NULL);
  }

  g_print ("Linking elements...\n");

  //  Creating Pipeline for Classification:
  //  qtiqmmfsrc (Camera) -> main_capsfilter -> tee (SPLIT)
  //      | tee -> qtivcomposer
  //      |     -> Pre process-> ML Framework -> Post process -> qtivcomposer
  //      qtivcomposer (COMPOSITION) -> waylandsink (Display)
  //      Pre process: qtimlvconverter
  //      ML Framework: qtimlsnpe/qtimltflite
  //      Post process: qtimlvpose -> detection_filter
  ret = gst_element_link_many (qtiqmmfsrc, main_capsfilter, queue[0], tee, NULL);
  if (!ret) {
    g_printerr ("Pipeline elements cannot be linked for qmmfsource->tee\n");
    goto error;
  }

  ret = gst_element_link_many (qtivcomposer, queue[1], waylandsink, NULL);
  if (!ret) {
    g_printerr ("Pipeline elements cannot be linked for"
        "qtivcomposer->waylandsink.\n");
    goto error;
  }

  ret = gst_element_link_many (tee, queue[2], qtivcomposer, NULL);
  if (!ret) {
    g_printerr ("Pipeline elements cannot be linked for tee->qtivcomposer.\n");
    goto error;
  }

  ret = gst_element_link_many (
      tee, queue[3], qtimlvconverter, queue[4],
      qtimlelement, queue[5], qtimlvpose,
      detection_filter, queue[6], qtivcomposer, NULL);
  if (!ret) {
    g_printerr ("Pipeline elements cannot be linked for"
        "pre proc -> ml framework -> post proc.\n");
    goto error;
  }

  return TRUE;

  // For any errors in plugin creation, cleanup earlier created ones
error:
  gst_bin_remove_many (GST_BIN (appctx->pipeline), qtiqmmfsrc, main_capsfilter,
      tee, qtivcomposer, qtimlvconverter, qtimlelement, qtimlvpose,
      detection_filter, waylandsink, NULL);

  for (int i = 0; i < QUEUE_COUNT; i++) {
    gst_bin_remove_many (GST_BIN (appctx->pipeline), queue[i], NULL);
  }

  return FALSE;
}

/**
 * Unlinks and removes all elements.
 *
 * @param appctx Application Context Pointer.
 */
static void
destroy_pipe (GstAppContext * appctx)
{
  GstElement *curr = (GstElement *) appctx->plugins->data;
  GstElement *next;

  GList *list = appctx->plugins->next;
  for ( ; list != NULL; list = list->next) {
    next = (GstElement *) list->data;
    gst_element_unlink (curr, next);
    gst_bin_remove (GST_BIN (appctx->pipeline), curr);
    curr = next;
  }
  gst_bin_remove (GST_BIN (appctx->pipeline), curr);

  g_list_free (appctx->plugins);
  appctx->plugins = NULL;
  gst_object_unref (appctx->pipeline);
}

/**
 * Main Function Of the Application.
 */
gint
main (gint argc, gchar * argv[])
{
  GMainLoop *mloop = NULL;
  GstBus *bus = NULL;
  GstElement *pipeline = NULL;
  GOptionContext *ctx = NULL;
  const gchar *model_path = NULL;
  const gchar *labels_path = DEFAULT_POSE_DETECTION_LABELS;
  const char *app_name = strrchr (argv[0], '/') ?
      (strrchr (argv[0], '/') + 1) : argv[0];
  GstAppContext appctx = {};
  ModelType model_type = MODEL_TYPE_TFLITE;
  guint intrpt_watch_id = 0;
  gboolean ret = FALSE;
  gchar help_description[1024];

  // Structure to define the user options selection
  GOptionEntry entries[] = {
    { "model", 'm', 0, G_OPTION_ARG_STRING,
      &model_path,
      "This is an optional parameter and overrides default path\n"
      "      Default model path for TFLITE Model: "
      DEFAULT_TFLITE_POSE_DETECTION_MODEL,
      "/PATH"
    },
    { "labels", 'l', 0, G_OPTION_ARG_STRING,
      &labels_path,
      "This is an optional parameter and overrides default path\n"
      "      Default labels path: " DEFAULT_POSE_DETECTION_LABELS,
      "/PATH"
    },
    { NULL }
  };

  snprintf (help_description, 1023, "\nExample:\n"
      "  %s --model=%s --labels=%s\n"
      "\nThis Sample App demonstrates Pose Detection on Live Stream",
      app_name, DEFAULT_TFLITE_POSE_DETECTION_MODEL,
      DEFAULT_POSE_DETECTION_LABELS);

  // Parse command line entries.
  if ((ctx = g_option_context_new (help_description)) != NULL) {
    gboolean success = FALSE;
    GError *error = NULL;

    g_option_context_add_main_entries (ctx, entries, NULL);
    g_option_context_add_group (ctx, gst_init_get_option_group ());

    success = g_option_context_parse (ctx, &argc, &argv, &error);
    g_option_context_free (ctx);

    if (!success && (error != NULL)) {
      g_printerr ("Failed to parse command line options: %s!\n",
          GST_STR_NULL (error->message));
      g_clear_error (&error);
      return -EFAULT;
    } else if (!success && (NULL == error)) {
      g_printerr ("Initializing: Unknown error!\n");
      return -EFAULT;
    }
  } else {
    g_printerr ("Failed to create options context!\n");
    return -EFAULT;
  }

  // Setting default model path for execution
  model_path = model_path ? model_path: DEFAULT_TFLITE_POSE_DETECTION_MODEL;

  g_print ("Running app with model: %s and labels: %s\n",
      model_path, labels_path);

  if (model_path == NULL || labels_path == NULL) {
    g_printerr ("Model or Labels cannot be null\n");
    return -EINVAL;
  }

  // Initialize GST library.
  argc = 1;
  gst_init (&argc, &argv);

  // Create the pipeline that will form connection with other elements
  pipeline = gst_pipeline_new (app_name);
  if (!pipeline) {
    g_printerr ("ERROR: failed to create pipeline.\n");
    return -1;
  }

  appctx.pipeline = pipeline;

  // Build the pipeline, link all elements in the pipeline
  ret = create_pipe (&appctx, model_type, model_path, labels_path);
  if (!ret) {
    g_printerr ("ERROR: failed to create GST pipe.\n");
    destroy_pipe (&appctx);
    return -1;
  }

  // Initialize main loop.
  if ((mloop = g_main_loop_new (NULL, FALSE)) == NULL) {
    g_printerr ("ERROR: Failed to create Main loop!\n");
    destroy_pipe (&appctx);
    return -1;
  }
  appctx.mloop = mloop;

  // Retrieve reference to the pipeline's bus.
  // Bus is message queue for getting callback from gstreamer pipeline
  if ((bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline))) == NULL) {
    g_printerr ("ERROR: Failed to retrieve pipeline bus!\n");
    g_main_loop_unref (mloop);
    destroy_pipe (&appctx);
    return -1;
  }

  // Watch for messages on the pipeline's bus.
  gst_bus_add_signal_watch (bus);

  // Call respective callback function based on message
  g_signal_connect (bus, "message::state-changed",
      G_CALLBACK (state_changed_cb), pipeline);

  g_signal_connect (bus, "message::error", G_CALLBACK (error_cb), mloop);

  g_signal_connect (bus, "message::eos", G_CALLBACK (eos_cb), mloop);
  gst_object_unref (bus);

  // Register function for handling interrupt signals with the main loop.
  intrpt_watch_id =
      g_unix_signal_add (SIGINT, handle_interrupt_signal, &appctx);

  // On successful transition to PAUSED state, state_changed_cb is called.
  // state_changed_cb callback is used to send pipeline to play state.
  g_print ("Setting pipeline to PAUSED state ...\n");
  switch (gst_element_set_state (pipeline, GST_STATE_PAUSED)) {
    case GST_STATE_CHANGE_FAILURE:
      g_printerr ("ERROR: Failed to transition to PAUSED state!\n");
      goto error;
    case GST_STATE_CHANGE_NO_PREROLL:
      g_print ("Pipeline is live and does not need PREROLL.\n");
      break;
    case GST_STATE_CHANGE_ASYNC:
      g_print ("Pipeline is PREROLLING ...\n");
      break;
    case GST_STATE_CHANGE_SUCCESS:
      g_print ("Pipeline state change was successful\n");
      break;
  }

  // Wait till pipeline encounters an error or EOS
  g_print ("g_main_loop_run\n");
  g_main_loop_run (mloop);
  g_print ("g_main_loop_run ends\n");

error:
  g_source_remove (intrpt_watch_id);
  g_main_loop_unref (mloop);

  g_print ("Setting pipeline to NULL state ...\n");
  gst_element_set_state (pipeline, GST_STATE_NULL);

  g_print ("Destory pipeline\n");
  destroy_pipe (&appctx);

  g_print ("gst_deinit\n");
  gst_deinit ();

  return 0;
}
